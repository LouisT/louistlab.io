---
layout: post
title:  "Learning Ruby and shortening Jekyll URLs."
date: 2016-05-31 16:27:26
categories: programming
tags: [ruby programming url shortener hash]
author: louist
image: /images/authors/louist-180x180.png
shorten: true
---

With my new domain ([lou.ist]) and the [switch to GitLab Pages], I have decided it is now time for me to learn a new programming language as well.
Because I can now use custom plugins for Jekyll, why not [Ruby]!? Seems like a great place to start expanding my knowledge.

For my first attempt, I decided that while the URLs Jekyll creates are pretty straight forward, they could be unnecessarily long at times.
This is where my [URL shortener]{:target="_blank"} comes in handy. Instead of a link like "[{{ page.url }}]({{ page.url }}){:target="_blank"}",
you can now get short and sweet links like "[/s/{{ page.short.hash }}]({{ page.short.url }}){:target="_blank"}".
While I realize the length of a URL isn't necessarily a huge deal for most people, I found it was a great starting point for my foray into the land of Ruby.

Check it out on GitLab: [https://gitlab.com/LouisT/jekyll-url-shortener][URL Shortener]{:target="_blank"}

[lou.ist]: https://lou.ist/
[switch to GitLab Pages]: https://lou.ist/blog/gitlab/2016/05/28/github-to-gitlab/
[Ruby]: https://www.ruby-lang.org/en/
[URL shortener]: https://gitlab.com/LouisT/jekyll-url-shortener
