---
layout: post
title:  "Switching from GitHub Pages to GitLab Pages with Jekyll."
date: 2016-05-28 16:20:14
categories: gitlab
tags: [github gitlab upgrade jekyll]
author: louist
image: /images/authors/louist-180x180.png
shorten: true
---

I've recently found the awesomeness that is [GitLab Pages][glp]{:target="_blank"} which has the ability to use [Shared Runners][runners]{:target="_blank"} to run custom [Jekyll]{:target="_blank"} plugins or any static generator that I wanted, really.
I had been using [GitHub Pages][ghp]{:target="_blank"} since sometime in 2014 and while it worked fine, it just didn't give me the freedom I desired. This is where GitLab Pages comes in handy!

My first step was to create a `username.gitlab.io` repository on [GitLab]{:target="_blank"}. While I could have easily imported the existing version from [GitHub]{:target="_blank"} I knew there were changes I needed to make. I then added my custom domain(s) under the newly created `username.gitlab.io` repository in `Settings > Pages > New Domain`. At which point I set my domain(s) as CNAME records of `username.gitlab.io` and that step was done.

Next, I created a local clone of my existing GitHub Pages repository. This is where I made a few needed changes. For my blog post listings, I am using pagination which any good blog should do. To support this I needed to modify my `_config.yml` file adding the following:

```yaml
gems: [jekyll-paginate]
```

After this, I created the required `.gitlab-ci.yml` in the repository base to trigger the shared runners for Jekyll.
Note that I had to include `jekyll-paginate` in this file as well, which the runner will install along with Jekyll, otherwise pagination wouldn't be available.

```yaml
image: ruby:2.3

pages:
  stage: deploy
  script:
  - gem install jekyll jekyll-paginate
  - jekyll build -d public
  artifacts:
    paths:
    - public
  only:
  - master
```

At this point I then made any changes to my site, set the origin, and was ready to commit.

```bash
$ git remote add origin git@gitlab.com:LouisT/louist.gitlab.io.git
$ git add .
$ git commit -m "Switching from GitHub Pages!"
$ git push --set-upstream origin master
```

Waited a few moments while the runner did its thing, and here we are! Everything moved from GitHub to GitLab with no issues.

[Jekyll]: http://jekyllrb.com/
[GitHub]: https://github.com/
[GitLab]: https://gitlab.com/
[ghp]: https://pages.github.com/
[glp]: https://pages.gitlab.io/
[runners]: https://about.gitlab.com/2016/04/05/shared-runners/
