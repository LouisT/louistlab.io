---
layout: post
title:  "New job, moving to California, life updates..."
date: 2017-07-27 15:41:15
categories: life
tags: [life job moving projects corefact]
author: louist
image: /images/authors/louist-180x180.png
shorten: true
location:
  latitude: 37.6688205
  longitude: -122.0807964
---

Recently I have been offered (and of course have accepted) a position at the printing company [Corefact] as a web developer doing [JavaScript]. While this is officially my first full-time position as a software engineer, I'm incredibly excited to finally start on an actual career path. Moving and adjusting to life in California is going to be a bit challenging, however I'd be crazy to pass up such an amazing opportunity. Which means I'm off to the Bay Area on August 15th, 2017.

{% google_map zoom="10" %}

Along with this (obviously major) life change, I have decided it's time to also better maintain my personal website as well as myself. While I might not post blogs daily (perhaps weekly? monthly?) nor the most exciting posts, I plan to at least share about my life in California, progress on my personal projects, how my time as a software engineer is going... who knows what else. If you have suggestions, let me know via my [contact] page.

It is time I put some real effort into my life and overall health. Going to join a Gym, (attempt) to eat healthier, work on social anxiety, make better overall choices and just in general be a better person. I'm going to be using this opportunity to work on and hopefully improve myself.

I don't have much else to say at this time, still in the process of figuring out... well, everything. However, I would like to give a huge thanks to everyone who has helped me get this far. Without all of you, I'd probably still be doing random manual labor jobs and freelance development just to get by. Glad that's all over with. Now it's time to focus on more a promising future.

Concentrating on me for now, let's see where this adventure leads.

[corefact]: https://www.corefact.com/
[javascript]: https://developer.mozilla.org/en-US/docs/Web/JavaScript
[contact]: /contact
