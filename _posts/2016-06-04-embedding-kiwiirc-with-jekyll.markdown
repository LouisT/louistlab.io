---
layout: post
title:  "Easily embed Kiwi IRC within Jekyll."
date: 2016-06-04 16:44:25
categories: programming
tags: [ruby programming kiwiirc irc]
author: louist
image: /images/authors/louist-180x180.png
shorten: true
---

I have been using [Kiwi IRC]{:target="blank"} for a while now and with its easy embedding features I have decided to create a Jekyll plugin for it.
Wanting to make this as simple as possible, I've decided to use [Tags]{:target="_blank"} to embed within pages/blog posts.
There are two methods you can choose between; A [widget]{:target="_blank"} or [custom client]{:target="_blank"}.

To configure the Kiwi IRC plugin, you can edit `_config.yml` to include the following:

```yaml
kiwiirc:
   custom:
      token: 3087b0899de5879c21147189d5cfd01b
   widget:
      channel: '#kiwiirc-default'
      server:  irc.kiwiirc.com
      port:    '+6697'
      nick:    kiwi?
```

Displaying Kiwi IRC in a page or blog post is as simple as:

```html
 # Embed a widget.
 {% raw %}{% kiwiirc_widget %}{% endraw %}
 # Output:
 {% kiwiirc_widget server: irc.kiwiirc.com, channel: #kiwiirc-default, nick: kiwi? %}

 # Embed the custom client.
 {% raw %}{% kiwiirc_custom %}{% endraw %}
 # Output:
 {% kiwiirc_custom token: 3087b0899de5879c21147189d5cfd01b %}
```

It is also possible to pass configuration within the tags:

```html
 # Embed a widget.
 {% raw %}{% kiwiirc_widget server: irc.voltirc.net, channel: #chat, nick: lt? %}{% endraw %}
 # Output:
 {% kiwiirc_widget server: irc.voltirc.net, channel: #chat, nick: lt? %}

 # Embed the custom client.
 {% raw %}{% kiwiirc_custom token: d7b61534ef45bb60e47579f69242927b %}{% endraw %}
 # Output:
 {% kiwiirc_custom token: d7b61534ef45bb60e47579f69242927b %}
```

You can see a working example on my [contact page].

Check it out on GitLab: [https://gitlab.com/LouisT/jekyll-kiwiirc][kiwiembed]{:target="_blank"}


[kiwi irc]: https://kiwiirc.com/
[tags]: https://jekyllrb.com/docs/plugins/#tags
[widget]: https://kiwiirc.com/embedding
[custom client]: https://kiwiirc.com/myclients
[kiwiembed]: https://gitlab.com/LouisT/jekyll-kiwiirc
[contact page]: /contact
