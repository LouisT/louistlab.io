# Jekyll URL Shortener
#
# Generates shortened URL links based on the SHA1 hash of the post date.
#
# Example post configuration:
#
#    ---
#     layout:  post
#     title:   "URL Shortener for Jekyll."
#     date:    1970-01-01 00:00:00           # The timestamp is converted to Unix time.
#     shorten: true                          # Shorten this post.
#    ---
#
# Exmaple _config.yml configuration:
#
#    shortener:
#       basepath: /s/        # Path for stored redirects.
#       preserve: false      # Keep previously generated files.
#       salt:     "LouisT"   # The optional salt, used when hasing the Unix time.
#       verbose:  true       # Output some basic shortening information.
#
# Author: Louis T.
# Site: https://lou.ist/
# Plugin Source: http://github.com/LouisT/jekyll-url-shortener
# Plugin License: MIT
require 'digest/sha1'
require 'fileutils'

module Jekyll
   class ShortenedFile < StaticFile
      def write (dest)
          super(dest) rescue ArgumentError
          true
      end
   end
   class URLShortener < Generator
      safe true
      priority :low
      def generate (site)
          @site = site

          @verbose = defined?(site.config['shortener']['verbose']) ? site.config['shortener']['verbose'] : true
          @start_time = Time.now.to_f if @verbose
          baseurl = site.config['baseurl'] ? site.config['baseurl'] : ''
          weburl = site.config['url'] ? site.config['url'] : ''
          shortpath = site.config['shortener']['shortpath'] ? site.config['shortener']['shortpath'] : '/s/'
          salt =  site.config['shortener']['salt'] ? site.config['shortener']['salt'] : ''
          Jekyll.logger.info "\nStarting URL shortener: #{shortpath}" if @verbose
          @site.posts.docs.each do |post|
             if post.data['shorten'] and post.data['date']
                hash = Digest::SHA1.new.hexdigest(post.data['date'].to_i.to_s + salt.to_s)[0...10]
                path = File.join(baseurl, shortpath, hash)
                dest = File.join(site.dest, path)
                file = File.join(dest, 'index.html')
                post.data['short'] = {
                   "url" => weburl+path,
                   "hash" => hash
                }
                Jekyll.logger.info "Shortening: #{shortpath}#{hash} -> #{post.url}" if @verbose
                unless File.directory?(dest)
                   FileUtils.mkdir_p(dest)
                   unless File.directory?(dest)
                      Jekyll.logger.error "Failed: #{shortpath}#{hash} -> #{post.url}" if @verbose
                      next
                   end
                 else
                   if site.config['shortener']['preserve']
                      return @site.static_files << ShortenedFile.new(@site, @site.dest, shortpath, file)
                   end
                end
                File.open(file, 'w') do |file|
                   file.write(template(post.url, post.data['title']).gsub(/\n\s+/, " ").strip)
                   file.close
                end
                if File.zero?(file)
                   Jekyll.logger.error "Failed: #{shortpath}#{hash} -> #{post.url}" if @verbose
                   next
                end
                @site.static_files << ShortenedFile.new(@site, @site.dest, shortpath, file)
             end
          end
          if (@verbose)
             finished = Time.now.to_f - @start_time
             Jekyll.logger.info "Finished shortening in #{finished} seconds.\n"
          end
      end
      def template (redirect, title)
          <<-EOF
          <!DOCTYPE html>
          <html>
           <head>
            <title>#{title}</title>
            <link rel="canonical" href="#{redirect}"/>
            <meta http-equiv="content-type" content="text/html; charset=utf-8" />
            <meta http-equiv="refresh" content="1; url = #{redirect}" />
           </head>
           <body>
            <h3>Redirecting to <a href="#{redirect}">#{redirect}</a></h3>
            <script>setTimeout(function () { window.location = "#{redirect}"; }, 1000);</script>
           </body>
          </html>
          EOF
      end
   end
end
